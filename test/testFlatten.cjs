
const nestedArray = [1, [2], [[3], 4], [[2]],, [[[2]]]];  // use this array to test your code.
const flatten = require('../flatten.cjs')
const test = require('assert')




let depth = 1;
console.log(flatten(nestedArray, depth))
test.deepEqual(flatten(nestedArray,depth),[ 1, 2, [ 3 ], 4, [ 2 ], [ [ 2 ] ] ])