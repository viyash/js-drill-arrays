const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.
const find = require('../find.cjs')
const test = require('assert')

function cb(element){
    return element % 2 === 0;
}


console.log(find(items,cb))
test.equal(find(items,cb), 2)