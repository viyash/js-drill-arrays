



function map(elements, functions) {
    let arr = []
    if (!Array.isArray(elements)) {
        return []
    }
    for (let index = 0; index < elements.length; index++) {
        arr.push(functions(elements[index], index, elements))
    }
    return arr
}


module.exports = map