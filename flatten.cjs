


let arr =[]
function flatten(elements, depth = 1) {
    for (let index = 0; index < elements.length; index++) {
        if (elements[index] == undefined) {
            continue;
        }
        if (Array.isArray(elements[index]) && depth > 0 ) {
            flatten(elements[index], depth - 1);
        }
        else if (!arr.includes(elements[index])) {
            arr.push(elements[index])
        }
    }
    return arr
}


module.exports = flatten