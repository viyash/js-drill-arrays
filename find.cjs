

function find(elements, functions) {

    for(let index=0;index<elements.length; index++){
        if (functions(elements[index])){
            return elements[index]
        }
    }
    return undefined
}


module.exports = find
