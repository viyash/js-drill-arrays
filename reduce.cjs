



function reduce(elements, cb, startingValue) {
    let accumulator = startingValue !== undefined ? startingValue : elements[0]
    const startIndex = startingValue !== undefined ? 0 : 1
    for (let index = startIndex; index < elements.length; index++) {
        accumulator = cb(accumulator, elements[index], index, elements)
    }
    return accumulator
}


module.exports = reduce
